import logo from "./logo.svg";
import "./App.css";
import { Profile } from "./Profile";
import Gallery from "./Gallery";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

function MyApp() {
  return (
    <div style={{ paddingLeft: "20px" }}>
      <h1>Hello World</h1>
      {/* <App /> */}
      <Profile
        person={{
          name: "Katherine Johnson",
          imageId: "MK3eW3As",
        }}
      />
      <Gallery />
    </div>
  );
}

export default MyApp;
