import { Avatar, DisplayType } from "./Avatar";

const data = [
  {
    id: "u001",
    name: "Katsuko Saruhashi",
    imageId: "YfeOqp2",
    bio: "Physicist professional",
  },
  {
    id: "u002",
    name: "Aklilu Lemma",
    imageId: "OKS67lh",
    bio: "Chemist professional",
  },
  {
    id: "u003",
    name: "Lin Lanying",
    imageId: "1bX5QH6",
    bio: "Mathematician professional",
  },
  {
    id: "u004",
    name: "Creola Katherine Johnson",
    imageId: "MK3eW3A",
    bio: "Spaceflight calculations",
  },
  {
    id: "u005",
    name: "Mario José Molina-Pasquel Henríquez",
    imageId: "mynHUSa",
    bio: "Discovery of Arctic ozone hole",
  },
  {
    id: "u006",
    name: "Mohammad Abdus Salam",
    imageId: "bE7W1ji",
    bio: "Electromagnetism theory",
  },
  {
    id: "u007",
    name: "Percy Lavon Julian",
    imageId: "IOjWm71",
    bio: "Pioneering cortisone drugs, steroids and birth control pills",
  },
  {
    id: "u008",
    name: "Subrahmanyan Chandrasekhar",
    imageId: "lrWQx8l",
    bio: "White dwarf star mass calculations",
  },
];

function PersonalItem(person) {
  return (
    <div className="profile-container">
      <div className="avatar-container">
        <Avatar
          imageId={person.imageId}
          description={person.name}
          displayType={DisplayType.AVATAR}
          size={80}
        />
      </div>
      <div className="profile-info">
        <h1 className="name">{person.name}</h1>
        <h3 className="bio">{person.bio}</h3>
      </div>
    </div>
  );
}

export default function Gallery() {
  return (
    <section>
      <h1>Amazing scientists</h1>
      <div>{data.map((p) => PersonalItem(p))}</div>
    </section>
  );
}
