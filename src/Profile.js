import { Avatar } from "./Avatar";
import { DisplayType } from "./Avatar";

export function Profile({ person }) {
  return (
    <>
      <h1>
        Hi <i>{person.name}</i>
      </h1>
      <Avatar imageId={person.imageId} description={person.name} />
    </>
  );
}
